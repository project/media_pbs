<?php

/**
 * @file media_pbs/themes/media-pbs-iframe.tpl.php
 *
 * The IFrame display for PBS videos.
 *
 * Available variables:
 *  $id => A unique, autoincremented number.
 *  $width => The width to display the player.
 *  $height => The height to display the player.
 *  $iframe_width => The width of the IFrame.
 *  $iframe_height => The height of the IFrame, with 40px added for the chapter
 *    bar, if required.
 *  $chapterbar => 'true' or 'false', do display the chapter bar.
 *  $chapter => The chapter to begin playing.
 *  $video_id => The ID of the PBS video.
 *  $autoplay => 'true' or 'false', to automatically start the player.
 *  $frameborder => The border for the IFrame.
 *  $marginwidth => The margin width of the IFrame.
 *  $marginheight => The margin height of the iFrame.
 *  $iframe_css => The CSS style overrides for the IFrame.
 */
?>
  <iframe
    id="media-pbs-iframe-<?php print $id; ?>"
    frameborder="<?php print $frameborder; ?>"
    marginwidth="<?php print $marginwidth; ?>"
    marginheight="<?php print $marginheight; ?>"
    scrolling="no"
    style="<?php print $iframe_css; ?>"
    src="http://video.pbs.org/widget/partnerplayer/<?php print $video_id; ?>/?w=<?php print $width; ?>&h=<?php print $height; ?>&chapterbar=<?php print $chapterbar; ?>&autoplay=<?php print $autoplay; ?>">
  </iframe>
