<?php

/**
 * @file media_pbs/themes/media-pbs-embed.tpl.php
 *
 * Template file for Media: PBS's theme('media_pbs_embed').
 *
 * This will display PBS's default embedded video.
 */
?>
  <object id="media-player-embed-<?php print $id; ?>" width = "<?php print $width; ?>" height = "<?php print $height; ?>" >
    <param name = "movie" value = "http://www-tc.pbs.org/video/media/swf/PBSPlayer.swf" > </param>
    <param name="flashvars" value="video=<?php print $video_id; ?>&player=viral&chapter=<?php print $chapter; ?>" />
    <param name="allowFullScreen" value="<?php print $fullscreen; ?>"></param >
    <param name = "allowscriptaccess" value = "always" > </param>
    <param name="wmode" value="transparent"></param >

    <embed src="http://www-tc.pbs.org/video/media/swf/PBSPlayer.swf"
      flashvars="video=<?php print $video_id; ?>&player=viral&chapter=<?php print $chapter; ?>"
      type="application/x-shockwave-flash"
      allowscriptaccess="always"
      wmode="transparent"
      allowfullscreen="<?php print $fullscreen; ?>"
      width="<?php print $width; ?>"
      height="<?php print $height; ?>"
      bgcolor="#000000">
    </embed>
  </object>
