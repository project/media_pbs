<?php

/**
 * @file media_pbs/themes/media-pbs.tpl.php
 *
 * Template file for Media: PBS's theme('media_pbs').
 *
 * This will display PBS's video, either as an IFrame or a direct object embed.
 */
?>
<div id="media-pbs-<?php print $id; ?>">
  <?php print $output; ?>
</div>
