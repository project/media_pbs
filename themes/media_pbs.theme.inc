<?php

/**
 * @file
 * Theme functions for Media: PBS.
 */

/**
 * Theme preprocess for theme('media_pbs').
 *
 * This will display either an IFrame or a direct object embed of the PBS video.
 */
function template_preprocess_media_pbs(&$variables) {
  // Merge in our default variables.
  $variables = array_merge($variables, $variables['variables']);

  $variables['iframe'] = isset($variables['iframe']) ? $variables['iframe'] : media_pbs_variable_get('iframe');

  $theme = $variables['iframe'] ? 'media_pbs_iframe' : 'media_pbs_embed';

  $variables['output'] = theme($theme, $variables);
}

/**
 * Theme preprocess for theme('media_pbs_embed').
 */
function template_preprocess_media_pbs_embed(&$variables) {
  _media_pbs_process_variables($variables);
}

/**
 * Theme preprocess for theme('media_pbs_iframe').
 */
function template_preprocess_media_pbs_iframe(&$variables) {
  _media_pbs_process_variables($variables);
}

/**
 * Helper function for the Media: PBS themes.
 */
function _media_pbs_process_variables(&$variables) {
  // Merge in our default variables.
  $variables = array_merge($variables, $variables['variables']);

  $variables['video_id'] = check_plain($variables['video_id']);

  foreach (array('fullscreen', 'chapterbar', 'autoplay') as $var) {
    $variables[$var] = _media_pbs_optional_boolean_to_string($var, $variables);
  }

  foreach (array('width', 'height', 'chapter', 'frameborder', 'marginwidth', 'marginheight') as $var) {
    $variables[$var] = isset($variables[$var]) ? check_plain($variables[$var]) : check_plain(media_pbs_variable_get($var));
  }

  $variables['iframe_width'] = isset($variables['iframe_width']) ? check_plain($variables['iframe_width']) : $variables['width'];
  $variables['adjust_for_chapterbar'] = isset($variables['adjust_for_chapterbar']) ? $variables['adjust_for_chapterbar'] : media_pbs_variable_get('adjust_for_chapterbar');

  if ($variables['chapterbar'] == 'true' && $variables['adjust_for_chapterbar']) {
    $variables['iframe_height'] = isset($variables['iframe_height']) ? check_plain($variables['iframe_height']) : $variables['height'] + media_pbs_variable_get('chapterbar_height');
  }
  else {
    $variables['iframe_height'] = isset($variables['iframe_height']) ? check_plain($variables['iframe_height']) : $variables['height'];
  }

  $variables['iframe_css'] = isset($variables['iframe_css']) ? check_plain($variables['iframe_css']) : "width:{$variables['iframe_width']}px;height:{$variables['iframe_height']}px;";

  $variables['scrolling'] = isset($variables['scrolling']) ? ((!$variables['scrolling'] || $variables['scrolling'] == 'no') ? 'no' : 'yes') : (media_pbs_variable_get('scrolling') ? 'yes' : 'no');
}

/**
 * Change a boolean value to 'true' or 'false'.
 *
 * @param string $variable_name
 *  The specific variable to test.
 * @param array $variables
 *  The variables array containing the required variable.
 *
 * @return string
 *  If the value or its default is 'false' or FALSE, then return 'false'.
 *  Otherwise, return 'true'.
 */
function _media_pbs_optional_boolean_to_string($variable_name, $variables) {
  return isset($variables[$variable_name]) ? ((!$variables[$variable_name] || $variables[$variable_name] == 'false') ? 'false' : 'true') : (media_pbs_variable_get($variable_name) ? 'true' : 'false');
}
