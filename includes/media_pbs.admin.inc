<?php

/**
 * @file media_pbs/includes/media_pbs.admin.inc
 * Administrative functions for Media: PBS.
 */

/**
 * The administrative settings form for Media: PBS.
 */
function media_pbs_settings() {
  $form = media_pbs_admin_form();
  $form['settings_info'] = array(
    '#type' => 'item',
    '#value' => t('These settings specifically affect videos displayed from !pbs. You can also read more about its !api.', array('!pbs' => l(t('pbs.org'), MEDIA_PBS_MAIN_URL, array('attributes' => array('target' => '_blank'))), '!api' => l(t("developer's API"), MEDIA_PBS_API_INFO, array('attributes' => array('target' => '_blank'))))),
    '#weight' => -10,
  );
  return system_settings_form($form);
}

/**
 * This form will be displayed both at /admin/settings/media_pbs and
 * admin/content/emfield.
 */
function media_pbs_admin_form() {
  $form = array();

  $form[media_pbs_variable_name('iframe')] = array(
    '#type' => 'checkbox',
    '#title' => t('Display using IFrame'),
    '#description' => t('If checked, display the video in an IFrame. This will allow slightly more customization, such as adding a chapter bar.'),
    '#default_value' => media_pbs_variable_get('iframe'),
  );
  $form[media_pbs_variable_name('chapterbar')] = array(
    '#type' => 'checkbox',
    '#title' => t('Display chapter bar'),
    '#description' => t('If checked, display the chapter bar for the video. Only include this if the episodes require that. This may be overridden on the theme level. Ignored if not displaying in an IFrame.'),
    '#default_value' => media_pbs_variable_get('chapterbar'),
  );
  $form[media_pbs_variable_name('adjust_for_chapterbar')] = array(
    '#type' => 'checkbox',
    '#title' => t('Adjust for chapter bar'),
    '#description' => t('If checked, adjust the IFrame for the chapterbar. Ignored if not displaying in an IFrame.'),
    '#default_value' => media_pbs_variable_get('adjust_for_chapterbar'),
  );
  $form[media_pbs_variable_name('chapterbar_height')] = array(
    '#type' => 'textfield',
    '#title' => t('Chapter bar height'),
    '#description' => t('This is the height of the chapter bar, in pixels, to be added to the IFrame if the above box is checked.'),
    '#default_value' => media_pbs_variable_get('chapterbar_height'),
  );
  $form[media_pbs_variable_name('chapter')] = array(
    '#type' => 'textfield',
    '#title' => t('Default chapter'),
    '#description' => t('This is the chapter of the video that PBS videos will begin by default, unless overridden in the theme. Ignored if displaying in an IFrame, due to limitations of the COVE API.'),
    '#default_value' => media_pbs_variable_get('chapter'),
  );
  $form[media_pbs_variable_name('fullscreen')] = array(
    '#type' => 'checkbox',
    '#title' => t('Fullscreen'),
    '#description' => t('If checked, this specifies that the flash may be played at full screen.'),
    '#default_value' => media_pbs_variable_get('fullscreen'),
  );
  $form[media_pbs_variable_name('width')] = array(
    '#type' => 'textfield',
    '#title' => t('Default width'),
    '#description' => t('This is the default width of the video, in pixels. Note that in most cases, this value will be overridden by the field settings. Note that the player looks best with a width of 512.'),
    '#default_value' => media_pbs_variable_get('width'),
  );
  $form[media_pbs_variable_name('height')] = array(
    '#type' => 'textfield',
    '#title' => t('Default height'),
    '#description' => t('This is the default height of the video, in pixels. Note that in most cases, this value will be overridden by the field settings. Note that the player looks best with a height of 288 with an IFrame, or 328 without.'),
    '#default_value' => media_pbs_variable_get('height'),
  );
  return $form;
}
