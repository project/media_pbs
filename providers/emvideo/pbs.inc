<?php

/**
 * @file
 *   This include processes pbs.com media files for use by emfield.module.
 */

/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_pbs_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Thumbnails'), t('No'), t('Still waiting on an answer from PBS re their COVE API.')),
    array(t('Duration'), t('No'), ''),
    array(t('Status'), t('No'), t('')),
    array(t('Custom player colors'), t('No'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
    array(t('Use JW FLV Media Player'), t('No'), t('')),
  );
  return array(
    'module' => 'media_pbs',
    'provides' => array('emvideo'),
    'provider' => 'pbs',
    'name' => t('PBS'),
    'url' => MEDIA_PBS_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from <a href="@pbs" target="_blank">PBS</a>. You can learn more about its <a href="@api" target="_blank">API</a> here.', array('@pbs' => MEDIA_PBS_MAIN_URL, '@api' => MEDIA_PBS_API_INFO)),
    'supported_features' => $features,
  );
}

/**
 * hook emvideo_PROVIDER_settings
 * this should return a subform to be added to the emvideo_settings() admin settings page.
 * note that a form field will already be provided, at $form['PROVIDER'] (such as $form['pbs'])
 * so if you want specific provider settings within that field, you can add the elements to that form field.
 */
function emvideo_pbs_settings() {
  module_load_include('inc', 'media_pbs', 'includes/media_pbs.admin');
  return media_pbs_admin_form();
}

/**
 *  Implement hook emvideo_PROVIDER_data_version().
 */
function emvideo_pbs_data_version() {
  return MEDIA_PBS_DATA_VERSION;
}

/**
 *  hook emfield_PROVIDER_rss
 *
 *  Embeds the video in the RSS feed.
 */
function emvideo_pbs_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file = array(
      'thumbnail' => array(
        'filepath' => media_pbs_thumbnail($item['value']),
      ),
    );
    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param string $url
 *   An optional string with the pasted URL or embed code
 * @return mixed
 *   Either an array of regex expressions to be tested, or a string with the
 *   video code to be used. If the hook tests the code itself, it should return
 *   either the string of the video code (if matched), or an empty array.
 *   Otherwise, the calling function will handle testing the embed code against
 *   each regex string in the returned array.
 */
function emvideo_pbs_extract($url = '') {
  // http://video.pbs.org/video/1619754531
  // <object width = "512" height = "328" > <param name = "movie" value = "http://www-tc.pbs.org/video/media/swf/PBSPlayer.swf" > </param><param name="flashvars" value="video=1619754531&player=viral&chapter=3" /> <param name="allowFullScreen" value="true"></param > <param name = "allowscriptaccess" value = "always" > </param><param name="wmode" value="transparent"></param ><embed src="http://www-tc.pbs.org/video/media/swf/PBSPlayer.swf" flashvars="video=1619754531&player=viral&chapter=3" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" allowfullscreen="true" width="512" height="328" bgcolor="#000000"></embed></object><p style="font-size:11px; font-family:Arial, Helvetica, sans-serif; color: #808080; margin-top: 5px; background: transparent; text-align: center; width: 512px;">Watch the <a style="text-decoration:none !important; font-weight:normal !important; height: 13px; color:#4eb2fe !important;" href="http://video.pbs.org/video/1619754531" target="_blank">full episode</a>. See more <a style="text-decoration:none !important; font-weight:normal !important; height: 13px; color:#4eb2fe !important;" href="http://www.pbs.org/art21/" target="_blank">ART:21.</a></p>
  // <iframe id="partnerPlayer" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="width:512px; height:328px;" src="http://video.pbs.org/widget/partnerplayer/1183001762/?w=512&h=288&chapterbar=true&autoplay=false"></iframe>
  return array(
    '@\.pbs\.org/video/([^/\? ]+)@i',
//     '@\.pbs\.org/program/([^/\? ]+)@i',
    '@\.pbs\.org/video/media/swf/PBSPlayer\.swf" > </param><param name="flashvars" value="video=([^\&" ]+)@i',
    '@\.pbs\.org/widget/partnerplayer/([^"\?/ ]+)@i',
  );
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site
 * @param string $video_code
 *   The string containing the video to watch
 * @return string
 *  A string containing the URL to view the video at the provider's site.
 */
function emvideo_pbs_embedded_link($video_code) {
  return media_pbs_video_url($video_code);
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * returns the external url for a thumbnail of a specific video
 * TODO: make the args: ($video_id, $field, $item), with $field/$item provided if we need it, but otherwise simplifying things
 *  @param $field
 *    the field of the requesting node
 *  @param $item
 *    the actual content of the field from the requesting node
 *  @return
 *    a URL pointing to the thumbnail
 */
function emvideo_pbs_thumbnail($field, $item, $formatter, $node, $width, $height, $options = array()) {
  return media_pbs_thumbnail($item['value']);
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the full/normal-sized video we want, usually on the default page view
 *  @param $video_id
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function emvideo_pbs_video($video_id, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $options['video_id'] = isset($options['video_id']) ? $options['video_id'] : $video_id;
  $options['width'] = isset($options['width']) ? $options['width'] : $width;
  $options['height'] = isset($options['height']) ? $options['height'] : $height;
  $options['field'] = isset($options['field']) ? $options['field'] : $field;
  $options['item'] = isset($options['item']) ? $options['item'] : $item;
  $options['node'] = isset($options['node']) ? $options['node'] : $node;
  $options['autoplay'] = isset($options['autoplay']) ? $options['autoplay'] : $autoplay;
  $output = theme('media_pbs', $options);
  return $output;
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the preview-sized video we want, commonly for the teaser
 *  @param $video_id
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function emvideo_pbs_preview($video_id, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $options['video_id'] = isset($options['video_id']) ? $options['video_id'] : $video_id;
  $options['width'] = isset($options['width']) ? $options['width'] : $width;
  $options['height'] = isset($options['height']) ? $options['height'] : $height;
  $options['field'] = isset($options['field']) ? $options['field'] : $field;
  $options['item'] = isset($options['item']) ? $options['item'] : $item;
  $options['node'] = isset($options['node']) ? $options['node'] : $node;
  $options['autoplay'] = isset($options['autoplay']) ? $options['autoplay'] : $autoplay;
  $output = theme('media_pbs', $options);
  return $output;
}

/**
 *  Implement hook_emvideo_PROVIDER_content_generate().
 */
function emvideo_pbs_content_generate() {
  return array();
}
