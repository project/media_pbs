
Media: PBS

Provides support for PBS videos to the Embedded Media Field module,
available at http://drupal.org/project/emfield. Install that and the included
Embedded Video Field module.
